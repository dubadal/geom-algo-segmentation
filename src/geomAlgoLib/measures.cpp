#include "measures.hpp"
#include <vector>
#include <map>
#include <math.h>
namespace geomAlgoLib
{

//calcule du périmetre de chaque face d'un maillage
    void face_perimeter(const Polyhedron& mesh, Facet_double_map * map_struct)
    {
        double perimetre = 0;
        for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i)
        {
            perimetre = 0;
            Halfedge_facet_circulator j = i->facet_begin();
            do {
                //calcule de la distance entre le point courant et le prochain
                double distance_squared = sqrt(CGAL::squared_distance(j->vertex()->point(), j->next()->vertex()->point()));
                perimetre += distance_squared;
                //std::cout << "Distance entre les sommets : " << distance_squared << std::endl;

            } while ( ++j != i->facet_begin());
            
            //std::cout << "périmètre face " << perimetre << std::endl;
            //std::cout << std::endl;

            map_struct->insert ( { i, perimetre } );
        }

    }

//calcule du plus petit angle de chaque face d'un maillage
    void face_smallest_angle(const Polyhedron& mesh, Facet_double_map * map_struct)
    {
        double min_angle = std::numeric_limits<double>::max(); // Initialisation avec une valeur maximale
        for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i)
        {
            min_angle = std::numeric_limits<double>::max();
            Halfedge_facet_circulator j = i->facet_begin();
            do {
                //calcule de l'angle entre le p1-p2 et p2-p3
                double angle = CGAL::approximate_angle(j->vertex()->point(), j->next()->vertex()->point(), j->next()->next()->vertex()->point());
                if (angle < min_angle)
                    min_angle = angle;
                
                //std::cout << "Angle entre les arretes : " << angle << std::endl;

            } while ( ++j != i->facet_begin());
            
            //std::cout << "min_angle face " << min_angle << std::endl;
            //std::cout << std::endl;

            map_struct->insert ( { i, min_angle } );
        }

    }

//calcule l'air de chaque face d'un maillage   
    void face_area(const Polyhedron& mesh, Facet_double_map * map_struct)
    {
        double area = 0;
        for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i)
        {
            area = 0;
            Halfedge_facet_circulator j = i->facet_begin();
            CGAL::Point_3 pt = j->vertex()->point();
            do {
                //calcule de l'air du triangel p1-p2-p3
                double part_area = sqrt(CGAL::squared_area(pt, j->vertex()->point(), j->next()->vertex()->point()));
                area += part_area;           
                //std::cout << "Aire triangle : " << part_area << std::endl;
                j++;

            } while ( j != i->facet_begin());
            
            //std::cout << "aire face " << area << std::endl;
            //std::cout << std::endl;

             map_struct->insert ( { i, area } );
        }

    }

    void classe_face(Facet_double_map * map_struct,Facet_string_map * string_map, double seuil,std::string personnal_label){
        std::string label ="";
        for (const auto & p : *(map_struct)){
            if (p.second > seuil)
			    label = personnal_label;
            else 
                label = "default";
            string_map->insert ( { p.first, label } );
        }
    }

    void angle_face_axeX(const Polyhedron& mesh, Facet_double_map * map_struct)
    {
        Kernel::Vector_3 axe_x = Kernel::Vector_3{1,0,0};
        angle_face_axe_generique(mesh, map_struct, axe_x);
    }

    void angle_face_axeY(const Polyhedron& mesh, Facet_double_map * map_struct)
    {
        Kernel::Vector_3 axe_y = Kernel::Vector_3{0,1,0};
        angle_face_axe_generique(mesh, map_struct, axe_y);
    }

    void angle_face_axeZ(const Polyhedron& mesh, Facet_double_map * map_struct)
    {
        Kernel::Vector_3 axe_z = Kernel::Vector_3{0,0,1};
        angle_face_axe_generique(mesh, map_struct, axe_z);
    }

    //remplie un dictionnaire qui associe chaque face avec l'angle de sa normal et du vecter axe passé en paramètre
    void angle_face_axe_generique(const Polyhedron& mesh, Facet_double_map * map_struct, Kernel::Vector_3 axe){
        Kernel::Vector_3 normale_facet = Kernel::Vector_3{0,0,0};   //vecteur normale de face
        double produit_scalaire = 0;                                //angle entre l'axe x et le veteur normale de la face
        double angle = 0;                                           //angle entre l'axe x et la normale a la surface
        
        for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i)
        {
            Halfedge_facet_circulator j = i->facet_begin();
        
            //calcule de l'angle entre l'axe X et la normal de la face
            normale_facet = CGAL::normal(j->vertex()->point(), j->next()->vertex()->point(), j->next()->next()->vertex()->point());
            produit_scalaire = CGAL::angle(normale_facet, axe);
            angle = acos(produit_scalaire);

            
            
            map_struct->insert ( { i, angle } );
        }


    }   
}