#pragma once

#include "types.hpp"

#include <CGAL/squared_distance_2.h> //for 2D functions
#include <CGAL/squared_distance_3.h> //for 3D functions

#include <iostream>
#include <fstream>

namespace geomAlgoLib
{
    // Computes perimeter of each faces and stores the value in a map
    void face_perimeter(const Polyhedron& mesh, Facet_double_map * map_struct);

    // Computes smallest angle of each faces and stores the value in a map
    void face_smallest_angle(const Polyhedron& mesh, Facet_double_map * map_struct);
    
    // Computes area of each faces and stores the value in a map
    void face_area(const Polyhedron& mesh, Facet_double_map * map_struct);

    void classe_face(Facet_double_map * map_struct,Facet_string_map * string_map, double seuil=0.5,std::string personnal_label="");
   
    //computes the angle between the normal and the x, y and z axes in a map
    void angle_face_axeX(const Polyhedron& mesh, Facet_double_map * map_struct);
    void angle_face_axeY(const Polyhedron& mesh, Facet_double_map * map_struct);
    void angle_face_axeZ(const Polyhedron& mesh, Facet_double_map * map_struct);

    void angle_face_axe_generique(const Polyhedron& mesh, Facet_double_map * map_struct, Kernel::Vector_3 axe);

}