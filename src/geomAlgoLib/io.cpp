#include "io.hpp"
#include <string>

namespace geomAlgoLib
{

bool readOFF(const std::string& filePath, Polyhedron& mesh){

    std::ifstream input(filePath);

	if (!input || !(input >> mesh) || mesh.is_empty())
	{
		std::cerr << "Invalid .OFF file" << std::endl;
		return false;
	}

    return true;

}

void writeOFF(const Polyhedron& mesh, const std::string& filePath)
{
	std::ofstream in_myfile;
	in_myfile.open(filePath);

	CGAL::set_ascii_mode(in_myfile);

	in_myfile << "COFF" << std::endl // "COFF" makes the file support color information
			  << mesh.size_of_vertices() << ' ' 
			  << mesh.size_of_facets() << " 0" << std::endl; 
			  // nb of vertices, faces and edges (the latter is optional, thus 0)

	std::copy(mesh.points_begin(), mesh.points_end(),
			  std::ostream_iterator<Kernel::Point_3>(in_myfile, "\n"));

	for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i)
	{
		Halfedge_facet_circulator j = i->facet_begin();

		CGAL_assertion(CGAL::circulator_size(j) >= 3);

		in_myfile << CGAL::circulator_size(j) << ' ';
		do
		{
			in_myfile << ' ' << std::distance(mesh.vertices_begin(), j->vertex());

		} while (++j != i->facet_begin());

		in_myfile << std::endl;
	}

	in_myfile.close();

	std::cout << "Successfully exported at path: " << filePath << " !" << std::endl;
}

void writeOFFColor(const Polyhedron& mesh, Facet_double_map * map_struct, const std::string& filePath)
{
	// On trouve le perimetre max des faces
	double max = 0.;
	for (const auto& pair : *map_struct) {
        if (pair.second > max) {
            max = pair.second;
        }
    }

	std::ofstream in_myfile;
	in_myfile.open(filePath);

	CGAL::set_ascii_mode(in_myfile);

	in_myfile << "COFF" << std::endl // "COFF" makes the file support color information
			  << mesh.size_of_vertices() << ' ' 
			  << mesh.size_of_facets() << " 0" << std::endl; 
			  // nb of vertices, faces and edges (the latter is optional, thus 0)

	// On recopie les sommets du mesh
	std::copy(mesh.points_begin(), mesh.points_end(),
			  std::ostream_iterator<Kernel::Point_3>(in_myfile, "\n"));

	// On recopie les faces du mesh
	for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i)
	{
		Halfedge_facet_circulator j = i->facet_begin();

		CGAL_assertion(CGAL::circulator_size(j) >= 3);

		in_myfile << CGAL::circulator_size(j) << ' ';
		do
		{
			in_myfile << ' ' << std::distance(mesh.vertices_begin(), j->vertex());

		} while (++j != i->facet_begin());

		// On ajoute la couleur des faces en fonction du max du mesh
		if (map_struct->at(i) == max)
			in_myfile << ' ' << 255 << " 0 0";
		else 
			in_myfile << ' ' << 0 << " 0 0";

		in_myfile << std::endl;
	}

	in_myfile.close();

	std::cout << "Successfully exported at path: " << filePath << " !" << std::endl;
}


void writeOFFColor_label(const Polyhedron& mesh, Facet_string_map * map_struct, const std::string& filePath)
{

	std::ofstream in_myfile;
	in_myfile.open(filePath);

	CGAL::set_ascii_mode(in_myfile);

	in_myfile << "COFF" << std::endl // "COFF" makes the file support color information
			  << mesh.size_of_vertices() << ' ' 
			  << mesh.size_of_facets() << " 0" << std::endl; 
			  // nb of vertices, faces and edges (the latter is optional, thus 0)

	// On recopie les sommets du mesh
	std::copy(mesh.points_begin(), mesh.points_end(),
			  std::ostream_iterator<Kernel::Point_3>(in_myfile, "\n"));

	// On recopie les faces du mesh
	for (Facet_iterator i = mesh.facets_begin(); i != mesh.facets_end(); ++i)
	{
		Halfedge_facet_circulator j = i->facet_begin();

		CGAL_assertion(CGAL::circulator_size(j) >= 3);

		in_myfile << CGAL::circulator_size(j) << ' ';
		do
		{
			in_myfile << ' ' << std::distance(mesh.vertices_begin(), j->vertex());

		} while (++j != i->facet_begin());

		// On ajoute la couleur des faces en fonction du max du mesh	
		if (map_struct->at(i) == "haut")
			in_myfile << " 0 0 " << 255;
		else if(map_struct->at(i) == "grand")
			in_myfile << " 0 " << 255 << " 0";

		in_myfile << std::endl;
	}

	in_myfile.close();

	std::cout << "Successfully exported at path: " << filePath << " !" << std::endl;
}


}