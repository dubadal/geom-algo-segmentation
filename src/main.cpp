#include <io.hpp>
#include <example.hpp>
#include <measures.hpp>

#include <iostream>
#include <string>
#include "types.hpp"

//retourne la valeur moyen du dictionnaire passé en paramètre
int found_average_threshold(geomAlgoLib::Facet_double_map * map_struct){
    // On trouve le perimetre max des faces
	double average = 0.;
	for (const auto& pair : *map_struct) {
        average += pair.second;
    }

    return average / map_struct->size();
}

int main(int argc, char *argv[]){

    std::cout << "Hello !" << std::endl;

    if(argc < 2){
        throw std::invalid_argument("This program expects at least 1 argument (path to a mesh).");
    }

    const std::string meshPath = std::string{argv[1]};
    
    geomAlgoLib::Polyhedron myMesh;

    geomAlgoLib::readOFF(meshPath, myMesh);

    auto genus = geomAlgoLib::computeGenus(myMesh);
    // std::cout << "The Genus of [" << meshPath << "] is = " << std::to_string(genus) << std::endl;

//dictionnaire pour les mesures local
    geomAlgoLib::Facet_double_map * my_dico_perimeter = new geomAlgoLib::Facet_double_map();
    geomAlgoLib::Facet_double_map * my_dico_angle = new geomAlgoLib::Facet_double_map();
    geomAlgoLib::Facet_double_map * my_dico_area = new geomAlgoLib::Facet_double_map();
    geomAlgoLib::Facet_double_map * my_dico_angle_normal_x = new geomAlgoLib::Facet_double_map();
    geomAlgoLib::Facet_double_map * my_dico_angle_normal_y = new geomAlgoLib::Facet_double_map();
    geomAlgoLib::Facet_double_map * my_dico_angle_normal_z = new geomAlgoLib::Facet_double_map();


//dictionnaire pour le stockage des labels
    geomAlgoLib::Facet_string_map * my_dico_label_perimetre = new geomAlgoLib::Facet_string_map();
    geomAlgoLib::Facet_string_map * my_dico_label_angle = new geomAlgoLib::Facet_string_map();
    geomAlgoLib::Facet_string_map * my_dico_label_area = new geomAlgoLib::Facet_string_map();
    geomAlgoLib::Facet_string_map * my_dico_label_angle_normal_x = new geomAlgoLib::Facet_string_map();
    geomAlgoLib::Facet_string_map * my_dico_label_angle_normal_y = new geomAlgoLib::Facet_string_map();
    geomAlgoLib::Facet_string_map * my_dico_label_angle_normal_z = new geomAlgoLib::Facet_string_map();

//calcule du périmetre des faces d'un polygone
    geomAlgoLib::face_perimeter(myMesh, my_dico_perimeter);

//calcule du plus petit angle de chaque face
    geomAlgoLib::face_smallest_angle(myMesh, my_dico_angle);

//calcule de l'aire de chaque face d'un polynome
    geomAlgoLib::face_area(myMesh, my_dico_area);

//remplisage des dictionnaires des angle entre les normal de la surface et les axe x, y et z pour toutes les face d'un polynome
    geomAlgoLib::angle_face_axeX(myMesh, my_dico_angle_normal_x);
    geomAlgoLib::angle_face_axeY(myMesh, my_dico_angle_normal_y);
    geomAlgoLib::angle_face_axeZ(myMesh, my_dico_angle_normal_z);

//remplisage d'une map d'association entre face et label en fonction d'un seuil
    int threshold = found_average_threshold(my_dico_perimeter); //retourne la valeur moyen du dictionnaire
    geomAlgoLib::classe_face(my_dico_perimeter, my_dico_label_perimetre, threshold,"grand");

    threshold = found_average_threshold(my_dico_angle);
    geomAlgoLib::classe_face(my_dico_angle, my_dico_label_angle, threshold,"grand");
    
    threshold = found_average_threshold(my_dico_area);
    geomAlgoLib::classe_face(my_dico_area, my_dico_label_area, threshold,"grand");
    
    threshold = found_average_threshold(my_dico_angle_normal_x);
    geomAlgoLib::classe_face(my_dico_angle_normal_x, my_dico_label_angle_normal_x, threshold,"haut");

    threshold = found_average_threshold(my_dico_angle_normal_y);
    geomAlgoLib::classe_face(my_dico_angle_normal_y, my_dico_label_angle_normal_y, threshold,"haut");

    threshold = found_average_threshold(my_dico_angle_normal_z);
    geomAlgoLib::classe_face(my_dico_angle_normal_z, my_dico_label_angle_normal_z, threshold,"haut");
    
//expore des maillages en exploitent les labels

//expore de maillage en couleur en utilisant des labels
    geomAlgoLib::writeOFFColor_label(myMesh, my_dico_label_perimetre, "color_face_max_peremiter.off");

    geomAlgoLib::writeOFFColor_label(myMesh, my_dico_label_angle, "color_smallest_angle.off");

    geomAlgoLib::writeOFFColor_label(myMesh, my_dico_label_area, "color_max_area.off");

//coloration en fonction de l'angle entre la normale de la face et l'axe x, y ou z
    geomAlgoLib::writeOFFColor_label(myMesh, my_dico_label_angle_normal_x, "color_max_orientation_x.off");
    geomAlgoLib::writeOFFColor_label(myMesh, my_dico_label_angle_normal_y, "color_max_orientation_y.off");
    geomAlgoLib::writeOFFColor_label(myMesh, my_dico_label_angle_normal_z, "color_max_orientation_z.off");

    threshold = found_average_threshold(my_dico_angle_normal_y);
    geomAlgoLib::classe_face(my_dico_angle_normal_y, my_dico_label_angle_normal_y, threshold,"haut");

    threshold = found_average_threshold(my_dico_angle_normal_z);
    geomAlgoLib::classe_face(my_dico_angle_normal_z, my_dico_label_angle_normal_z, threshold,"haut");
    
    //expore des maillages en exploitent les labels

//expore de maillage en couleur en utilisant des labels
    geomAlgoLib::writeOFFColor_label(myMesh, my_dico_label_perimetre, "color_face_max_peremiter.off");

    geomAlgoLib::writeOFFColor_label(myMesh, my_dico_label_angle, "color_smallest_angle.off");

    geomAlgoLib::writeOFFColor_label(myMesh, my_dico_label_area, "color_max_area.off");

//coloration en fonction de l'angle entre la normale de la face et l'axe x, y ou z
    geomAlgoLib::writeOFFColor_label(myMesh, my_dico_label_angle_normal_x, "color_max_orientation_x.off");
    geomAlgoLib::writeOFFColor_label(myMesh, my_dico_label_angle_normal_y, "color_max_orientation_y.off");
    geomAlgoLib::writeOFFColor_label(myMesh, my_dico_label_angle_normal_z, "color_max_orientation_z.off");





/*
//expore de maillage en couleur en fonction de différentre messue locale
    geomAlgoLib::writeOFFColor(myMesh, my_dico_perimeter, "color_face_max_peremiter.off");

    geomAlgoLib::writeOFFColor(myMesh, my_dico_angle, "color_smallest_angle.off");

    geomAlgoLib::writeOFFColor(myMesh, my_dico_area, "color_max_area.off");

//coloration en fonction de l'angle entre la normale de la face et l'axe x, y ou z
    geomAlgoLib::writeOFFColor(myMesh, my_dico_angle_normal_x, "color_max_orientation_x.off");
    geomAlgoLib::writeOFFColor(myMesh, my_dico_angle_normal_y, "color_max_orientation_y.off");
    geomAlgoLib::writeOFFColor(myMesh, my_dico_angle_normal_z, "color_max_orientation_z.off");
*/
    std::cout << "The end..." << std::endl;

    return 0;
}

