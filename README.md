# ZZ3F1 Géométrie Algorithmique 2024



## Mise en place du projet

Récupérer le projet sur le Gitlab ISIMA :

```
git clone https://gitlab.isima.fr/dubadal/geom-algo-segmentation.git
```

Créer un dossier build
```
mkdir build
```

Compiler le projet
```
cd build
cmake ..
make
```

Lancer l'application en présisant le chemin du mesh
```
./mainApp chemin_mesh
```
